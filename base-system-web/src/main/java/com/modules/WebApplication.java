package com.modules;

import com.common.web.util.SpringContextHolder;
import com.common.web.util.YmlConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The type Web admin application.
 *
 * @author dcp
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableTransactionManagement //启用事务
@ComponentScan(basePackages = "com.modules")
@Import(value = {SpringContextHolder.class, YmlConfig.class})
public class WebApplication extends SpringBootServletInitializer {

    /**
     * Logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(WebApplication.class);

//    @Override
//    public void onStartup(ServletContext servletContext) throws ServletException {
//        servletContext.setInitParameter("spring.profiles.active", "dev");
//        servletContext.setInitParameter("spring.profiles.default", "dev");
//        servletContext.setInitParameter("spring.liveBeansView.mbeanDomain", "dev");
//        super.onStartup(servletContext);
//    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(this.getClass());
    }


    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(WebApplication.class);
        application.setBannerMode(Banner.Mode.OFF);
        application.run(args);
    }
}
