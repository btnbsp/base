package com.modules.system.mapper;

import com.common.mapper.BaseMapper;
import com.modules.system.entity.SysNotice;
import org.apache.ibatis.annotations.Mapper;
/**
 * 通知公告表
 * 
 * @author henrycao
 * @email 631079326@qq.com
 * @date 2017-06-12 23:15:41
 */
@Mapper
public interface SysNoticeMapper extends BaseMapper<SysNotice> {
	
}
