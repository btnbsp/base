package com.modules.system.service;

import com.common.service.BaseService;
import com.modules.system.entity.SysAttachmentInfo;

/**
 * 系统附件表
 *
 * @author xmh
 * @email 
 * @date 2018-07-29 10:41:23
 */
public interface SysAttachmentInfoService extends BaseService<SysAttachmentInfo> {

}
