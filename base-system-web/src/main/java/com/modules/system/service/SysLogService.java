package com.modules.system.service;

import com.common.service.BaseService;
import com.modules.system.entity.SysLog;

/**
 * 操作日志
 */
public interface SysLogService extends BaseService<SysLog> {

}
