package com.modules.system.service.impl;

import com.common.service.BaseServiceImpl;
import com.modules.system.entity.SysLog;
import com.modules.system.mapper.SysLogMapper;
import com.modules.system.service.SysLogService;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements SysLogService {


}
