import React, {Fragment} from 'react';
import {Link, Redirect, Route, Switch} from 'dva/router';
import DocumentTitle from 'react-document-title';
import {Icon} from 'antd';
import GlobalFooter from '../components/GlobalFooter';
import styles from './UserLayout.less';
import {getRoutes} from '../utils/utils';
import logo from '../assets/logo.svg';
import {copyrightText, logoText, name} from '../utils/config';

const copyright = (
  <Fragment>
    Copyright <Icon type="copyright" /> {new Date().getFullYear() +' '+ copyrightText}
  </Fragment>
);

class UserLayout extends React.PureComponent {
  getPageTitle() {
    const { routerData, location } = this.props;
    const { pathname } = location;
    let title = name;
    if (routerData[pathname] && routerData[pathname].name) {
      title = `${routerData[pathname].name} - ${name}`;
    }
    return title;
  }
  render() {
    const { routerData, match } = this.props;
    return (
      <DocumentTitle title={this.getPageTitle()}>
        <div className={styles.container}>
          <div className={styles.content}>
            <div className={styles.top}>
              <div className={styles.header}>
                <Link to="/">
                  <img alt={logoText} className={styles.logo} src={logo} />
                  <span className={styles.title}>{name}</span>
                </Link>
              </div>
              <div className={styles.desc}></div>
            </div>
            <Switch>
              {getRoutes(match.path, routerData).map(item => (
                <Route
                  key={item.key}
                  path={item.path}
                  component={item.component}
                  exact={item.exact}
                />
              ))}
              <Redirect exact from="/sys" to="/sys/login" />
            </Switch>
          </div>
          <GlobalFooter copyright={copyright} />
        </div>
      </DocumentTitle>
    );
  }
}

export default UserLayout;
